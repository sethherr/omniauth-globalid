# coding: utf-8
$:.push File.expand_path("../lib", __FILE__)
require "omniauth/globalid/version"

Gem::Specification.new do |s|
  s.name = "omniauth-globalid"
  s.version = OmniAuth::Globalid::VERSION
  s.authors = ["Seth Herr"]
  s.homepage = "https://gitlab.com/globalid/opensource/omniauth-globalid"
  s.description = %q{OmniAuth strategy for GlobaliD}
  s.summary = s.description
  s.license = "ISC"

  s.files = `git ls-files`.split("\n").reject { |f| f.start_with?("spec/") }
  s.executables = `git ls-files -- bin/*`.split("\n").map { |f| File.basename(f) }
  s.require_paths = ["lib"]
  s.required_ruby_version = Gem::Requirement.new(">= 2.2")

  s.add_runtime_dependency "omniauth", "~> 1.2"
  s.add_runtime_dependency "omniauth-oauth2", "~> 1.1"
  s.add_runtime_dependency "jwt", "~> 2.2"
  s.add_dependency "rack"
  s.add_development_dependency "bundler", "~> 1.14"
  s.add_development_dependency "rake", "~> 12.0"
end
