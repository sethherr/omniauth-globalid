# frozen_string_literal: true

module OmniAuth
  module Globalid
    VERSION = "0.1.4"
  end
end
