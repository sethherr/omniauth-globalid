require "spec_helper"

describe OmniAuth::Strategies::Globalid do
  let(:callback_url) { "example.com" }
  let(:request) { double("Request", params: {}, cookies: {}, env: {}) }

  subject do
    args = ["appid", "secret", @options || {}].compact
    OmniAuth::Strategies::Globalid.new(*args).tap do |strategy|
      allow(strategy).to receive(:callback_url) { callback_url } # Required by callback_url method in strategy
      allow(strategy).to receive(:session) { {} }
      allow(strategy).to receive(:request) {
        request
      }
    end
  end

  # Get the URL params from the request_phase
  let(:authentication_request_url) { subject.request_phase[1]["Location"] }
  let(:authentication_request_params) { Rack::Utils.parse_nested_query(URI.parse(authentication_request_url).query) }

  describe "client options" do
    it "has the correct name" do
      expect(subject.options.name).to eq("globalid")
    end
  end

  describe "urls" do
    it "has the correct urls" do
      expect(subject.options.client_options.site).to eq("https://auth.global.id")
      expect(subject.options.client_options.authorize_url).to eq("/")
      expect(subject.options.client_options.token_url).to eq("https://api.globalid.net/v1/auth/token")
    end
  end

  describe "authorize url" do
    it "gets a state" do
      expect(authentication_request_params["state"]).to_not be_nil
    end
    describe "nonce is inserted" do
      it "adds a nonce if scope is openid" do
        @options = { scope: "openid public" } # currently multi scopes are not supported, but prepping for future
        expect(authentication_request_params["nonce"]).to_not be_nil
      end
    end
    describe "acrc_id" do
      before { @options = { acrc_id: "some-acrc" } }
      it "uses config acrc_id" do
        expect(authentication_request_params["acrc_id"]).to eq "some-acrc"
      end
      context "request parameter with acrc_id" do
        let(:request) { double("Request", params: { acrc_id: "a-different-acrc" }, cookies: {}, env: {}) }
        it "uses request acrc_id" do
          expect(authentication_request_params["acrc_id"]).to eq "a-different-acrc"
        end
      end
    end
    context "passed private_key" do
      it "has the correct scope" do
        @options = { private_key: "some key", acrc_id: "some-acrc" }
        expect(authentication_request_params["scope"]).to eq "openid"
      end
      context "multiple scopes" do
        xit "not currently supported"
      end
    end
  end

  describe "acrc_id" do
    it "uses the configuration"
    context "passed in params" do
      it "uses the parameters version"
    end
  end

  describe "id_token" do
    let(:access_token_json) { File.read("spec/fixtures/openid_connect_access_token.json") }
    it "parses the values from the id token" do
      allow(subject).to receive(:raw_info) { {} } # Stub out API request
      allow(subject).to receive(:access_token) { JSON.parse(access_token_json) }
      expect(subject.info[:id_token].keys).to eq(%w[sub iss nonce iat exp idp.globalid.net/claims/dd24263d-079b-4779-9776-167fe6e03ab8 idp.globalid.net/claims/null])
    end
  end
end
